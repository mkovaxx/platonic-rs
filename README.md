# Platonic

Generates the five Platonic solids in OBJ format.

Usage: `platonic <FACES>`

```
FACES  SOLID
    4  tetrahedron
    6  hexahedron (cube)
    8  octahedron
   12  dodecahedron
   20  icosahedron
```
