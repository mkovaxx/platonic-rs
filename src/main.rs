use std::collections::HashMap;

extern crate cgmath;
use cgmath::InnerSpace;
use cgmath::Zero;

#[macro_use]
extern crate clap;

fn main() {
    let matches = clap::App::new("platonic")
        .version("0.0.1")
        .author("Mate Kovacs <mkovaxx@gmail.com>")
        .about("Generates the five Platonic solids in OBJ format")
        .arg(clap::Arg::with_name("FACES")
            .help("Number of faces")
            .possible_values(&["4", "6", "8", "12", "20"])
            .required(true)
            .index(1)
        )
        .get_matches();

    let faces: u32 = value_t!(matches, "FACES", u32).unwrap();

    let mesh = match faces {
        4 => make_tetrahedron(),
        6 => make_hexahedron(),
        8 => make_octahedron(),
        12 => make_dodecahedron(),
        20 => make_icosahedron(),
        _ => panic!("unimplemented solid"),
    };

    print_obj(&mesh);
}

fn print_obj(mesh: &Mesh) {
    println!("# vertices");
    for v in &mesh.vertices {
        println!("v {} {} {}", v.x, v.y, v.z);
    }
    println!("# faces");
    for f in &mesh.faces {
        print!("f");
        for i in f {
            print!(" {}", i+1);
        }
        println!("");
    }
}

type Vertex = cgmath::Vector3<f32>;
type Face = Vec<usize>;

struct Mesh {
    vertices: Vec<Vertex>,
    faces: Vec<Face>,
}

fn make_tetrahedron() -> Mesh {
    let vertices = normalize(vec!(
        [-1.0, -1.0, -1.0],
        [ 1.0,  1.0, -1.0],
        [-1.0,  1.0,  1.0],
        [ 1.0, -1.0,  1.0],
    ));
    let faces = triangulate(&vertices, 3.0);
    Mesh { vertices, faces }
}

fn make_hexahedron() -> Mesh {
    dual(&make_octahedron())
}

fn make_octahedron() -> Mesh {
    let vertices = normalize(vec!(
        [-1.0,  0.0,  0.0],
        [ 1.0,  0.0,  0.0],
        [ 0.0, -1.0,  0.0],
        [ 0.0,  1.0,  0.0],
        [ 0.0,  0.0, -1.0],
        [ 0.0,  0.0,  1.0],
    ));
    let faces = triangulate(&vertices, 3.0);
    Mesh { vertices, faces }
}

fn make_dodecahedron() -> Mesh {
    dual(&make_icosahedron())
}

fn make_icosahedron() -> Mesh {
    let phi: f32 = 0.5 * (1.0 + f32::sqrt(5.0));
    let vertices = normalize(vec!(
        [ 0.0, -phi, -1.0],
        [ 0.0, -phi,  1.0],
        [ 0.0,  phi, -1.0],
        [ 0.0,  phi,  1.0],
        [-1.0,  0.0, -phi],
        [-1.0,  0.0,  phi],
        [ 1.0,  0.0, -phi],
        [ 1.0,  0.0,  phi],
        [-phi, -1.0,  0.0],
        [-phi,  1.0,  0.0],
        [ phi, -1.0,  0.0],
        [ phi,  1.0,  0.0],
    ));
    let faces = triangulate(&vertices, 2.0);
    Mesh { vertices, faces }
}

fn normalize(vertices: Vec<[f32; 3]>) -> Vec<Vertex> {
    vertices.iter().map(|cs| {
        Vertex::new(cs[0], cs[1], cs[2]).normalize()
    }).collect()
}

fn triangulate(vertices: &Vec<Vertex>, threshold: f32) -> Vec<Face> {
    let mut faces: Vec<Face> = vec!();
    let n: usize = vertices.len();
    for ai in 0 .. n {
        let a = &vertices[ai];
        for bi in ai+1 .. n {
            let b = &vertices[bi];
            for ci in bi + 1 .. n {
                let c = &vertices[ci];
                let ab = b - a;
                let bc = c - b;
                let ca = a - c;
                if  ab.magnitude2() < threshold &&
                    bc.magnitude2() < threshold &&
                    ca.magnitude2() < threshold {
                    faces.push(if a.dot(Vertex::cross(ab, ca)) < 0.0 {
                        vec!(ai, bi, ci)
                    } else {
                        vec!(ai, ci, bi)
                    });
                }
            }
        }
    }
    faces
}

fn dual(mesh: &Mesh) -> Mesh {
    let mut vertex_to_edge : HashMap<usize, (usize, usize)> = HashMap::new();
    let mut edge_to_face: HashMap<(usize, usize), usize> = HashMap::new();
    let mut vertex_face_to_prev: HashMap<(usize, usize), usize> = HashMap::new();
    for (fi, face) in mesh.faces.iter().enumerate() {
        for (i, vi) in face.iter().enumerate() {
            let j: usize = (i + 1) % face.len();
            let vj: usize = face[j];
            let edge = (*vi, vj);
            vertex_to_edge.insert(*vi, edge);
            edge_to_face.insert(edge, fi);
            vertex_face_to_prev.insert((vj, fi), *vi);
        }
    }

    let vertices = mesh.faces.iter().map(|face| {
        face.iter()
            .fold(Vertex::zero(), |c, &vi| c + mesh.vertices[vi])
            .normalize()
    }).collect();
    let faces = mesh.vertices.iter().enumerate().map(|(ref vi, _)| {
        let mut face: Face = vec!();
        // start with any outgoing half-edge
        let edge = vertex_to_edge.get(vi).unwrap();
        let mut e: (usize, usize) = edge.clone();
        // enumerate outgoing half-edges around the vertex, collecting the faces on their left
        loop {
            let fi: &usize = edge_to_face.get(&e).unwrap();
            face.push(fi.clone());
            e.1 = *vertex_face_to_prev.get(&(*vi, fi.clone())).unwrap();
            if e.1 == edge.1 {
                break;
            }
        }
        face
    }).collect();
    Mesh { vertices, faces }
}
